#!/bin/sh

PROJECT=$1
echo $PROJECT
NAMESPACE=wavefier-stable
CHART_NAME=${PROJECT}
helm upgrade --install ${CHART_NAME} --namespace ${NAMESPACE} $PROJECT
