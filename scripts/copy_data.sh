#!/bin/sh

# This is a temporary workaround
# to copy influxdb and grafana configuration

NAMESPACE="wavefier-stable"
RELEASE_NAME="services"

# Get the running pods
INFLUX_POD=`kubectl get pods -n ${NAMESPACE} | grep ${RELEASE_NAME}-influxdb | awk '{print $1}'`
GRAFANA_POD=`kubectl get pods -n ${NAMESPACE} | grep ${RELEASE_NAME}-grafana | awk '{print $1}'`
 echo $INFLUX_POD 
# Copy data
kubectl cp  storage/influxdb ${NAMESPACE}/${INFLUX_POD}:/var/lib/
kubectl cp  storage/grafana ${NAMESPACE}/${GRAFANA_POD}:/var/lib/

# Restart the pods
kubectl delete pod ${INFLUX_POD} -n ${NAMESPACE}
kubectl delete pod ${GRAFANA_POD} -n ${NAMESPACE}

