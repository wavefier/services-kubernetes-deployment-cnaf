#!/bin/sh

NAMESPACE=wavefier-stable
PROJECT=$1
CHART_NAME=$PROJECT
helm uninstall --namespace ${NAMESPACE} ${CHART_NAME}

