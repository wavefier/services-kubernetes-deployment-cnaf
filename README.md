# Kubernetes Deployment at CNAF

Deploy the services needed by Wavefier (Kafka, InfluxDB, Grafana and third party importers) on Kubernetes. The target infrastructure is the Virgo Kubernetes cluster hosted on the CNAF Cloud.
All the components are installed via Helm charts. Scripts for the manual installation and deletetion of the Helm charts are provided.

The complete documentation can be found here: https://wavefier.gitlab.io/services-kubernetes-deployment-cnaf/
