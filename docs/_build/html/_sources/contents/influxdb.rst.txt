*******************
Influxdb deployment
*******************

.. figure:: ../_static/Chrono.png
   :align: center

   Chronograf graphical user interface to query Influxdb.

A first Helm chart installs a StetefulSet with one replica that mounts a PersistentVolumeClaim of 10 GiB to store the database data and configuration. 
To install the chart, first edit the install script to modify the Namespace and then run it:

.. highlight:: bash
.. code-block:: bash

   ./scripts/install.sh influxdb

A second Helm chart installs the Chronograf GUI to query the database. 
To install the chart, first edit the install script to modify the Namespace and then run it:

.. highlight:: bash
.. code-block:: bash

   ./scripts/install.sh chronograf

StatefulSet and Deployment are associated with a Service each, in order to allow for inbound connectivity.

