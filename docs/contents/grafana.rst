******************
Grafana deployment
******************

.. figure:: ../_static/Grafana.png
   :align: center

   Wavefier's Grafana dashboard.

StatefulSet with one replica that mounts a PersistentVolumeClaim of 10 GiB to store the configuration (i.e. dashboards). 
The StatefulSet is associated with a Service, in order to allow for inbound connectivity. 
To install the chart, first edit the install script to modify the Namespace and then run it:

.. highlight:: bash
.. code-block:: bash

   ./scripts/install.sh grafana

