*****************
Temporary patches
*****************

Copy the directories containig configuration and data into the Influxdb and Grafana containers:

.. highlight:: bash
.. code-block:: bash

   ./scripts/copy_data.sh
