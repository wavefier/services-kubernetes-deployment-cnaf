****************
Kafka deployment
****************

.. figure:: ../_static/Kowl.png
   :align: center

   Kowl graphical user interface to Kafka.

The Helm chart installs four Deployments of one replica each: Kafka, Kafka SchemaRegistry, Zookeeper and the Kowl GUI.
Each Deployment is associated with a Service, in order to allow for inbound connections from the other components. In this simple  implementation, no persistent storage is provided.
To install the chart, first edit the install script to modify the Namespace and then run it:

.. highlight:: bash
.. code-block:: bash

   ./scripts/install.sh kafka

