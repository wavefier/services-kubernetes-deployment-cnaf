******************************
The Kubernetes cluster at CNAF
******************************

.. figure:: ../_static/Rancher_dashboard.png
   :align: center
   
   Rancher dashboard of the Kubernetes cluster.

Deploy the services needed by Wavefier (Kafka, InfluxDB, Grafana and third party importers) on Kubernetes.
The target infrastructure is the Virgo Kubernetes cluster  hosted on the CNAF Cloud (region Tier1). 
The cluster is managed via Rancher and counts 12 schedulable nodes, for a total of 96 cpu cores, 186 GiB of memory and about 2 TiB of gross storage. 
These resources are shared among Wavefier and other test deployments of LVK low-latency services: GraceDB, LValert and GWCelery.
All the components are installed as Kubernetes Deployments or StatefulSets via Helm charts. 
Scripts for the manual installation and deletion of the Helm charts are provided.
