*******************
Access the services
*******************

Grafana, Chronograf and Kowl pages can be accessed from the CNAF internal network. An Nginx VirtualServer for Wavefier is deployed on the Kubernetes cluster and we need to install the VirtualServerRoutes for the desired services. This is achieved by running the command: 

.. highlight:: bash
.. code-block:: bash
   
   kubectl apply -f wavefier-stable_Routes.yaml

The services will be accessible at:

* http://wavefier.cloud.cnaf.infn.it/wavefier-stable/grafana
* http://wavefier.cloud.cnaf.infn.it/wavefier-stable/chronograf
* http://wavefier.cloud.cnaf.infn.it/wavefier-stable/kowl
