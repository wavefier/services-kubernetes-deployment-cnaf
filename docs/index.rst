.. services-kubernetes-deployment-cnaf documentation master file, created by
   sphinx-quickstart on Mon Mar 14 16:55:10 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to services-kubernetes-deployment-cnaf's documentation!
===============================================================
Deploy the services needed by Wavefier (Kafka, InfluxDB, Grafana and third party importers) on Kubernetes.

.. toctree::
   :maxdepth: 2
   :caption: Table of contents

   contents/intro.rst
   contents/setup.rst
   contents/kafka.rst
   contents/influxdb.rst
   contents/grafana.rst
   contents/ingress.rst
   contents/patches.rst
